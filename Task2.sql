--1. Используемая база данных Northwind. Написать скрипт на выборку всех товаров с ценой в диапазоне [15,30).
SELECT * 
FROM 
	Products
WHERE 
	UnitPrice >= 15 AND UnitPrice < 30

--2. Используемая база данных Northwind. Написать скрипт на выборку всех товаров начинающихся на латинскую букву «с»
SELECT * 
FROM 
	Products
WHERE 
	CHARINDEX('c', ProductName) = 1

--3. Используемая база данных Northwind. Написать скрипт на выборку всех товаров имеющих в названии вторую латинскую букву «b».
SELECT * 
FROM 
	Products
WHERE 
	CHARINDEX('b', ProductName) = 2

-- 4. Используемая база данных Northwind. Написать скрипт на выборку всех товаров, в названии которых встречается последовательность «eso».
SELECT * 
FROM 
	Products
WHERE 
	CHARINDEX('eso', ProductName) > 0

--5. Используемая база данных Northwind. Написать скрипт на выборку всех товаров, название которых заканчивается на «es».
SELECT * 
FROM 
	Products
WHERE 
	RIGHT(ProductName, 2) = 'es'

--6. Используемая база данных Northwind. Написать скрипт на выборку записей о продажах. (Детализация – какой товар, количество, цена продажи)
SELECT 
	p.ProductName as 'Наименование товара', od.Quantity as 'Количество', od.UnitPrice as 'Цена продажи'
FROM 
	[Order Details] as od JOIN Products as p ON od.ProductID = p.ProductID

--7. Используемая база данных Northwind. Написать скрипт на выборку записей о продажах. (Детализация – какой товар, количество, цена продажи, дата продажи, покупатель)
SELECT 
	p.ProductName as 'Наименование товара', od.Quantity as 'Количество', od.UnitPrice as 'Цена продажи', o.OrderDate as 'Дата продажи', c.ContactName as 'Покупатель'
FROM 
	[Order Details] as od JOIN Products as p ON od.ProductID = p.ProductID 
		JOIN Orders as o ON od.OrderID = o.OrderID 
		JOIN Customers as c ON o.CustomerID = c.CustomerID

--8. Используемая база данных Northwind. Написать скрипт на выборку записей о продажах. (Детализация – какой товар, количество, цена продажи, дата продажи, покупатель). Результат отсортирован по наименованию товара, прямая сортировка.
SELECT 
	p.ProductName as 'Наименование товара', od.Quantity as 'Количество', od.UnitPrice as 'Цена продажи', o.OrderDate as 'Дата продажи', c.ContactName as 'Покупатель'
FROM 
	[Order Details] as od JOIN Products as p ON od.ProductID = p.ProductID 
		JOIN Orders as o ON od.OrderID = o.OrderID 
		JOIN Customers as c ON o.CustomerID = c.CustomerID
ORDER BY p.ProductName

--9. Используемая база данных Northwind. Написать скрипт на выборку записей о продажах, за 1997 год. (Детализация – какой товар, количество, цена продажи, дата продажи). Результат отсортирован по цене, обратная сортировка.
SELECT
	p.ProductName as 'Товар', od.Quantity as 'Количество', od.UnitPrice as 'Цена продажи', o.OrderDate as 'Дата продажи'
FROM 
	Orders as o JOIN [Order Details] as od ON od.OrderID = o.OrderID
	JOIN Products as p ON p.ProductID = od.ProductID
WHERE
	YEAR(o.OrderDate) = 1997
ORDER BY od.UnitPrice DESC

--10. Используемая база данных Northwind. Написать скрипт на выборку записей о продажах, сделанных до 1997 года. (Детализация – какой товар, количество, цена продажи, дата продажи). Результат отсортирован по цене, обратная сортировка.
SELECT
	p.ProductName as 'Товар', od.Quantity as 'Количество', od.UnitPrice as 'Цена продажи', o.OrderDate as 'Дата продажи'
FROM 
	Orders as o JOIN [Order Details] as od ON od.OrderID = o.OrderID
	JOIN Products as p ON p.ProductID = od.ProductID
WHERE
	YEAR(o.OrderDate) < 1997
ORDER BY od.OrderID DESC

--11. Используемая база данных Northwind. Написать скрипт на выборку первых 7 записей о продажах. (Детализация – какой товар, сумма, дата продажи, покупатель). Результат отсортирован по цене, прямая сортировка.
SELECT TOP 7
	p.ProductName as 'Товар', od.UnitPrice*od.Quantity as 'Сумма', o.OrderDate as 'Дата продажи', c.ContactName as 'Покупатель'
FROM 
	Orders as o JOIN [Order Details] as od ON od.OrderID = o.OrderID
	JOIN Products as p ON p.ProductID = od.ProductID
	JOIN Customers as c ON c.CustomerID = o.CustomerID
ORDER BY od.OrderID 

--12. Используемая база данных Northwind. Написать скрипт на выборку всех записей о суммарных продажах каждого товара. (Детализация – какой товар, сумма). Результат отсортирован по сумме, обратная сортировка.
SELECT 
	p.ProductName as 'Товар', SUM(od.UnitPrice*od.Quantity) as 'Сумма'
FROM 
	[Order Details] as od JOIN Products as p ON p.ProductID = od.ProductID
GROUP BY p.ProductName
ORDER BY Сумма DESC

--13. Используемая база данных Northwind. Написать скрипт на выборку всех записей о количестве продаж каждого товара. (Детализация – товар, количество). Результат отсортирован по сумме, обратная сортировка.
SELECT 
	p.ProductName as 'Товар', SUM(od.Quantity) as 'Количество'
FROM  Products as p JOIN [Order Details] as od ON p.ProductID = od.ProductID
GROUP BY p.ProductName
ORDER BY SUM(od.UnitPrice*od.Quantity) DESC

--14. Используемая база данных Northwind. Написать скрипт на выборку всех записей о суммарных продажах каждого товара за 1997 год. (Детализация – какой товар, сумма). Результат отсортирован по сумме, обратная сортировка.
SELECT 
	p.ProductName as 'Товар', SUM(od.UnitPrice*od.Quantity) as 'Сумма'
FROM 
	Orders as o JOIN [Order Details] as od ON od.OrderID = o.OrderID
	JOIN Products as p ON p.ProductID = od.ProductID
WHERE
	YEAR(o.OrderDate) = 1997
GROUP BY p.ProductName
ORDER BY Сумма DESC

--15. Используемая база данных Northwind. Написать скрипт на выборку первых 8 записей о суммарных продажах каждого товара за 1997 год. (Детализация – какой товар, сумма). Результат отсортирован по сумме, обратная сортировка.
SELECT TOP 8
	p.ProductName as 'Товар', SUM(od.UnitPrice*od.Quantity) as 'Сумма'
FROM 
	Orders as o JOIN [Order Details] as od ON od.OrderID = o.OrderID
	JOIN Products as p ON p.ProductID = od.ProductID
WHERE
	YEAR(o.OrderDate) = 1997
GROUP BY p.ProductName
ORDER BY Сумма DESC

--16. Используемая база данных Northwind. Написать скрипт на выборку всех записей о суммарных покупках каждого покупателя, на сумму большую 500. (Детализация – покупатель, сумма). Результат отсортирован по сумме, обратная сортировка.
SELECT 
	c.ContactName as 'Покупатель', SUM(od.UnitPrice*od.Quantity) as 'Сумма'
FROM 
	Orders as o JOIN Customers as c ON o.CustomerID = c.CustomerID
	JOIN [Order Details] as od ON o.OrderID = od.OrderID
GROUP BY c.ContactName
HAVING SUM(od.UnitPrice*od.Quantity) > 500
ORDER BY Сумма DESC 

--17. Используемая база данных Northwind. Написать скрипт на выборку всех записей о суммарных покупках каждого покупателя, на сумму меньшую 1 000. (Детализация – покупатель, сумма). Результат отсортирован по сумме, обратная сортировка.
SELECT 
	c.ContactName as 'Покупатель', SUM(od.UnitPrice*od.Quantity) as 'Сумма'
FROM 
	Orders as o JOIN Customers as c ON o.CustomerID = c.CustomerID
	JOIN [Order Details] as od ON o.OrderID = od.OrderID
GROUP BY c.ContactName
HAVING SUM(od.UnitPrice*od.Quantity) < 1000
ORDER BY Сумма DESC 