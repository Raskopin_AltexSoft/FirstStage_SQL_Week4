-- CREATING NEW DATABASE
CREATE 
DATABASE AutopartsStoreDB
ON
(
	NAME = 'AutopartsStoreDB',
	FILENAME = 'F:\AltexSoftLab\Week4\AutopartsStoreDB.mdf',
	SIZE = 5MB,
	MAXSIZE = 10MB,
	FILEGROWTH = 1MB
)
LOG ON
(
	NAME = 'LogAutopartsStoreDB',
	FILENAME = 'F:\AltexSoftLab\Week4\AutopartsStoreDB.ldf',
	SIZE = 5MB,
	MAXSIZE = 10MB,
	FILEGROWTH = 1MB
)

USE AutopartsStoreDB

--CREATING TABLES
CREATE TABLE S 
(
	n_post char(5) not NULL,
	name char(20),
	reiting smallint,
	town char(15)
)

CREATE TABLE P
(
	n_det char(6),
	name char(20),
	cvet char(7),
	ves smallint,
	town char(15)
)

CREATE TABLE SP
(
	n_post char(5),
	n_det char(6),
	date_post date,
	kol smallint
)

--FILLING TABLES
INSERT INTO S (n_post, name, reiting, town)
VALUES
('S1', 'Смит',  20, 'Лондон'),
('S2', 'Джонс', 10, 'Париж'),
('S3', 'Блейк', 30, 'Париж'),
('S4', 'Кларк', 20, 'Лондон'),
('S5', 'Адамс', 30, 'Атенс')

INSERT INTO P (n_det, name, cvet, ves, town)
VALUES
('P1', 'Гайка',  'Красный', 12, 'Лондон'),
('P2', 'Болт',   'Зеленый', 17, 'Париж'),
('P3', 'Винт',   'Голубой', 17, 'Рим'),
('P4', 'Винт',   'Красный', 14, 'Лондон'),
('P5', 'Кублачок','Голубой', 12, 'Париж'),
('P6', 'Блюм',   'Красный', 19, 'Лондон')


INSERT INTO SP (n_post, n_det, date_post, kol)
VALUES
('S1', 'P1', '02/01/95', 300),
('S1', 'P2', '04/05/95', 200),
('S1', 'P3', '05/12/95', 400),
('S1', 'P4', '06/15/95', 200),
('S1', 'P5', '07/22/95', 100),
('S1', 'P6', '08/13/95', 100),
('S2', 'P1', '03/03/95', 300),
('S2', 'P2', '06/12/95', 400),
('S3', 'P2', '04/04/95', 200),
('S4', 'P2', '03/23/95', 200),
('S4', 'P4', '06/17/95', 300),
('S4', 'P5', '08/22/95', 400)

--01. Выдать полную информацию о поставщиках.
SELECT 
	n_post as 'Номер поставщика', name as 'Фамилия', reiting as 'Рейтинг', town as 'Город' 
FROM S


--01. Выдать таблицу S в следующем порядке: фамилия, город, рейтинг, номер_поставщика
SELECT 
	name as 'Фамилия', town as 'Город', reiting as 'Рейтинг' , n_post as 'Номер поставщика'
FROM S

--02. Выдать номера всех поставляемых деталей. 
SELECT 
	n_det as 'Номер детали'
FROM P

--03. Выдать номера всех поставляемых деталей, исключая дублирование.
SELECT DISTINCT 
	n_det as 'Номер детали'
FROM P

--04. Выдать сокращения фамилий до двух букв и рейтинг поставщиков
SELECT 
	LEFT(name, 2) as name, reiting
FROM S

--05. Выдать поставщиков, упорядоченных по городу, в пределах города - по рейтингу
SELECT
	n_post as 'Номер поставщика', name as 'Фамилия', reiting as 'Рейтинг', town as 'Город'
FROM S
ORDER BY town, reiting

--06. Выбрать список деталей, начинающихся с буквы "Б"
SELECT
	n_det as 'номер_детали', name as 'название',  ves as 'вес'
FROM P
WHERE CHARINDEX('Б', name) = 1

--07. Выдать средний, минимальный и максимальный объем поставок для 
--поставщика S1 с соответствующим заголовком
SELECT CAST(AVG(CAST(kol as float)) as decimal(10,1)) as average, MIN(kol) as minimum, MAX(kol) as maximum
FROM SP
WHERE n_post = 'S1'

--08. Выдать перечень поставок и их количество, а также день, месяц, день недели и количество дней, 
--прошедших с момента поставки на сегодняшний день. 
SELECT 
	n_post as 'Номер детали', 
	kol as 'Количество', 
	DAY(date_post) as 'Day', 
	MONTH(date_post) as 'Month', 
	DATEPART(WEEKDAY, date_post) as 'Weekday',
	DATEDIFF(DAY, date_post, GETDATE()) as 'DaysAcount'
FROM SP

--09. Выдать все комбинации информации о поставщиках и деталях, расположенных в одном городе. 
SELECT 
	S.n_post as 'н_пост', 
	S.name as 'фамилия', 
	S.reiting as 'рейтинг', 
	S.town as 'город поставщика', 
	P.n_det as 'н_дет', 
	P.cvet as 'цвет', 
	P.ves as 'вес', 
	P.town as 'город детали'
FROM SP, S, P
WHERE SP.n_post = S.n_post AND SP.n_det = P.n_det AND S.town = P.town

--10. Выдать для каждой поставляемой детали ее номер и общий объем поставок, за исключением поставо поставщика S1.
SELECT 
	P.n_det, SUM(SP.kol) as '(SUM)'
FROM 
	SP JOIN S ON SP.n_post = S.n_post
	INNER JOIN P ON SP.n_det = P.n_det
WHERE S.n_post <> 'S1' 
GROUP BY P.n_det

SELECT 
	P.n_det, SUM(SP.kol) as '(SUM)'
FROM 
	P, SP, S
WHERE S.n_post <> 'S1' AND SP.n_post = S.n_post AND SP.n_det = P.n_det
GROUP BY P.n_det

--11. Выдать номера деталей, которые имеют вес более 16 фунтов, либо поставляются поставщиком S2.
SELECT 
	P.n_det
FROM 
	P JOIN SP ON P.n_det = SP.n_det
WHERE 
	P.ves > 16 OR SP.n_post = 'S2'

--12. Удалить все поставки для поставщиков из Лондона.
--Результат: таблица SP с отсутствующими строками о поставках для поставщиков из Лондона. 
DELETE 
	SP 
FROM 
	SP JOIN S ON SP.n_post = S.n_post
WHERE
	S.town = 'Лондон'

